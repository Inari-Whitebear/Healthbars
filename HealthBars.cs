﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Storm.ExternalEvent;
using Storm.StardewValley.Event;
using System;
using System.IO;

namespace InariWhitebear.Healthbars
{
    [Mod]
    public class HealthBars : DiskResource
    {
        Texture2D tex;
        Texture2D texBar;
        Color colWhite;
        Color colRed;
        Color colWhiteOpaque;

        Config config;
        String configFile = "config.json";

        [Subscribe]
        public void Initalize(InitializeEvent ev)
        {
            config = Config.FromFile(Path.Combine(PathOnDisk, configFile));

            int innerBarWidth = config.BarWidth - config.BarBorderWidth * 2;
            int innerBarHeight = config.BarHeight - config.BarBorderHeight * 2;
            tex = ev.Root.LoadResource(Path.Combine(PathOnDisk, "chatBox.png"));
            texBar = new Texture2D(ev.Root.Graphics.GraphicsDevice, innerBarWidth, innerBarHeight);
            var data = new uint[innerBarWidth * innerBarHeight];
            for (int i = 0; i < data.Length; i++)
            {
                data[i] = 0xffffffff;
            }
            texBar.SetData<uint>(data);

            colWhite = new Color(255, 255, 255, 200);
            colWhiteOpaque = new Color(255, 255, 255, 255);
            colRed = new Color(150, 0, 0, 200);
        }

        [Subscribe]
        public void PostRender(PreUIRenderEvent ev)
        {
            var gameLoc = ev.Root.CurrentLocation;
            if (gameLoc == null) return;
            var monsters = gameLoc.Monsters;
            if (monsters == null) return;

            var font = ev.Root.SmoothFont;
            var batch = ev.Root.SpriteBatch;
            var viewport = ev.Root.Viewport;

            foreach (var monster in monsters)
            {
                if (monster.MaxHealth < monster.Health)
                {
                    monster.MaxHealth = monster.Health;
                }
                if (monster.MaxHealth == monster.Health) continue;

                var animSprite = monster.Sprite;

                var size = new Vector2(animSprite.SpriteWidth, animSprite.SpriteHeight) * ev.Root.PixelZoom;
                var screenLoc = monster.Position - new Vector2(viewport.X, viewport.Y);
                screenLoc.X += size.X / 2 - config.BarWidth / 2.0f;
                screenLoc.Y -= config.BarHeight;

                var innerBarWidth = config.BarWidth - config.BarBorderWidth * 2;
                var innerBarHeight = config.BarHeight - config.BarBorderHeight * 2;

                var fill = monster.Health / (float)monster.MaxHealth;
                
                batch.Draw(
                    tex,
                    screenLoc,
                    tex.Bounds,
                    colWhite,
                    0.0f,
                    Vector2.Zero,
                    new Vector2((float)config.BarWidth / tex.Bounds.Width, (float)config.BarHeight / tex.Bounds.Height),
                    SpriteEffects.None,
                    0);
                batch.Draw(
                    texBar,
                    screenLoc + new Vector2(config.BarBorderWidth, config.BarBorderHeight),
                    texBar.Bounds,
                    colRed,
                    0.0f,
                    Vector2.Zero,
                    new Vector2(fill, 1.0f),
                    SpriteEffects.None,
                    0);

                var textLeft = monster.Health.ToString();
                var textRight = monster.MaxHealth.ToString();
                var textSizeL = font.MeasureString(textLeft);
                var textSizeR = font.MeasureString(textRight);

                batch.DrawString(
                    font,
                    textLeft,
                    screenLoc - new Vector2(0.0f, textSizeL.Y + 1.0f),
                    colWhiteOpaque,
                    0.0f,
                    Vector2.Zero,
                    1.0f,
                    SpriteEffects.None,
                    0);
                batch.DrawString(
                    font,
                    textRight,
                    screenLoc + new Vector2(config.BarWidth, 0.0f) - new Vector2(textSizeR.X, textSizeR.Y + 1.0f),
                    colWhiteOpaque,
                    0.0f,
                    Vector2.Zero,
                    1.0f,
                    SpriteEffects.None,
                    0);
            }
        }
    }
}
