﻿using Newtonsoft.Json;
using Storm;
using System;
using System.IO;

namespace InariWhitebear.Healthbars
{
    public class Config
    {
        public int BarWidth { get; set; }
        public int BarHeight { get; set; }

        public int BarBorderWidth { get; set; }
        public int BarBorderHeight { get; set; }

        public Config(string path)
        {
            BarWidth = 90;
            BarHeight = 15;
            BarBorderWidth = 2;
            BarBorderHeight = 2;
            save(path);
        }

        public void save(string path)
        {
            try {
                using (var ofs = File.OpenWrite(path))
                {
                    using (var writer = new StreamWriter(ofs))
                    {
                        writer.Write(JsonConvert.SerializeObject(this, Formatting.Indented));
                    }
                }
                
            }
            catch (Exception ex)
            {
                Logging.DebugLog("Failed to save config file: " + path + " / " + ex.Message);
            }
        }

        public static Config FromFile(string path)
        {
            try {
                if (File.Exists(path))
                {
                    using (var ifs = File.OpenRead(path))
                    {
                        using (var reader = new StreamReader(ifs))
                        {
                            return JsonConvert.DeserializeObject<Config>(reader.ReadToEnd());
                        }
                    }
                }
            }
            catch(Exception ex) {
                Logging.DebugLog("Failed to open/read config file " + path + ", using default values. (Error: " + ex.Message + ")");
            }

            return new Config(path);
        }
    }
}
